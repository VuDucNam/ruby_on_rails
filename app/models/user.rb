class User < ApplicationRecord
  has_many :lessons
  has_many :active_relationships, foreign_key: "follower_id",
                                  class_name: Follow.name,
                                  dependent: :destroy
  has_many :passive_relationships, class_name:  Follow.name,
                                   foreign_key: "followed_id",
                                   dependent: :destroy
  has_many :followings, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum sex: [:male, :female]
end
