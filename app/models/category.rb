class Category < ApplicationRecord
  has_many :words

  validates :name, presence: true,
                   length: { maximum: Settings.validate.category.name.max_length },
                   uniqueness: { case_sensitive: false }

  scope :latest, -> { order(created_at: :desc) }
end
