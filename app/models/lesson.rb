class Lesson < ApplicationRecord
  belongs_to :user
  has_many :lesson_words
  has_many :words, through: :lesson_words
end
