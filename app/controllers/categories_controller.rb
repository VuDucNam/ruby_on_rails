class CategoriesController < ApplicationController
  def index
    @categories = Category.latest.page(params[:page]).per Settings.pagination.category.user
  end
end
