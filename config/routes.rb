Rails.application.routes.draw do
  root "static_pages#home"
  devise_for :users, :controllers => { registrations: "registrations" }
  resources :categories, only: %i(index show)

  namespace :admin do
    resources :categories
    resources :static_pages, only: %i(index)
  end

end
