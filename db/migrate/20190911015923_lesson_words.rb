class LessonWords < ActiveRecord::Migration[6.0]
  def change
    create_table :lesson_words do |t|
      t.references :lesson, foreign_key: true
      t.references :word, foreign_key: true
      t.integer :answer_id

      t.timestamps
    end
  end
end
