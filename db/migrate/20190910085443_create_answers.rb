class CreateAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :answers do |t|
      t.references :word, foreign_key: true
      t.string :answers
      t.boolean :is_correct

      t.timestamps
    end
  end
end
